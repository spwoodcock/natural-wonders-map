# Natural Wonders Map

**My first major dev project back in 2017**.

Cross-platform Kivy framework mobile application, for displaying natural wonder information around the world.

Published on Play Store: https://play.google.com/store/apps/details?id=com.naturalwondersmap.android
An error on Android >6 on release = bad reviews 😢
